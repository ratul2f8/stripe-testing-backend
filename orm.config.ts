import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
const dbConfig: PostgresConnectionOptions = {
  database: process.env.POSTGRES_DATABASE,
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: +process.env.POSTGRES_PORT,
  password: process.env.POSTGRES_PASSWORD,
  username: process.env.POSTGRES_USER,
  entities: ['dist/src/**/*.entity.js'],
  // entities: ['src/**/*.entity.ts'],
  synchronize: false,
  logging: true,
  migrations: [
    // 'src/db/migrations/*.ts'
    'dist/src/db/migrations/**/*.js',
  ],
  cli: {
    migrationsDir: 'src/db/migrations',
  },
};
export default dbConfig;
