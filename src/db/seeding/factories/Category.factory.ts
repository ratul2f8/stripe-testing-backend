import { CategoryEntity } from 'src/category/entities/category.entity';
import { define } from 'typeorm-seeding';

define(CategoryEntity, () => {
  const category = new CategoryEntity();
  return category;
});
