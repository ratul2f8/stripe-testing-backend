import { CategoryEntity } from 'src/category/entities/category.entity';
import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';

export class CategorySeed implements Seeder {
  categories = ['Kitchen', 'Bathroom', 'Bedroom', 'Drawingroom', 'Diningroom'];
  async run(factory: Factory, connection: Connection): Promise<void> {
    for (let i = 0; i < this.categories.length; i++) {
      try {
        await factory(CategoryEntity)().create({
          name: this.categories[i],
        });
      } catch (e) {}
    }
  }
}
