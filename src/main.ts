import {
  VersioningType,
  ValidationPipe,
  ValidationError,
  UnprocessableEntityException,
} from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: '1',
  });
  app.setGlobalPrefix('api');
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
      exceptionFactory: (errors: ValidationError[]) => {
        const messages = {};

        errors.forEach((obj) => {
          messages[obj.property] =
            obj.constraints[Object.keys(obj.constraints)[0]];
        });

        throw new UnprocessableEntityException({
          messages,
          statusCode: 422,
        });
      },
    }),
  );
  app.enableCors();
  const swaggerConfig = new DocumentBuilder()
    .setTitle('NestJS API docs')
    .setDescription('The description of the APIs')
    .setVersion('1.0')
    .addBearerAuth(
      {
        description: `[just text field] Please enter token in following format: Bearer <JWT>`,
        name: 'Authorization',
        bearerFormat: 'Bearer',
        scheme: 'Bearer',
        type: 'http',
        in: 'Header',
      },
      'access-token',
    )
    .build();

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('/api-docs', app, document);
  await app.listen(process.env.PORT || 5000);
}
bootstrap();
