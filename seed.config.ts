export default {
    database: process.env.POSTGRES_DATABASE,
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    password: process.env.POSTGRES_PASSWORD,
    username: process.env.POSTGRES_USER,
    seeds: ['dist/src/db/seeding/seeds/**/*.js'],
    factories: ['dist/src/db/seeding/factories/**/*.js'],
    entities: ['dist/src/**/*.entity.js'],
  };
  